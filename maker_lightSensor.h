#ifndef _MAKER_LIGHTSENSOR_
#define _MAKER_LIGHTSENSOR_

/*           .
         .   :   .  
     '.   .  :  .   .'
  ._   '._.-'''-._.'   _.
    '-..'         '..-' 
 --._ /.==.     .==.\ _.--
     ;/_o__\   /_o__\;
-----|`     ) (     `|-----
    _: \_) (\_/) (_/ ;_
 --'  \  '._.=._.'  /  '--
   _.-''.  '._.'  .''-._
  '    .''-.(_).-''.    '
     .'   '  :  '   '.
        '    :   '
             '

*/

#include "Arduino.h"
#include "stdint.h"


/*
    Note: Reads the sensor value from the analog pin.

    @param
      pin is the analog pin from which to read the value.

    @return
      the value read from the pin.
*/
uint8_t maker_getLight(uint8_t pin); 

/*
    Note: Reads the sensor value from the analog pin and calculates the illumination percentage.

    @param
      pin is the pin from which to read the value.
      if b is true then the fuction returns the percentage

    @return
      the value read from the pin turned into percentage
*/
double maker_getLight_P(uint8_t pin); 

#endif
