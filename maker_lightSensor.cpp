/*
 * Library: maker_lightSensor.h
 *
 * Organization: MakerRobotics
 * Autors: Luka Caric
 *
 * Date: 1.12.2017.
 * Test: Arduino UNO
 *
 */

#include "Arduino.h"
#include "stdint.h"
#include "maker_lightSensor.h"

/* Returns the analog value */
uint8_t maker_getLight(uint8_t pin)	 
{
	return analogRead(pin);
}

/* Returns the calculated percentage */
double maker_getLight_P(uint8_t pin)	
{
	return  (double)analogRead(pin)/1024*100;
	
}
